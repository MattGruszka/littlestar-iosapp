//
//  ViewController.swift
//  
//
//  Created by Gruszkovsky on 27.03.2016.
//  Copyright © 2016 MateuszGruszka. All rights reserved.
//

import UIKit
import QuartzCore

class ViewController: UIViewController {
    
    var targetValue = 0
    var currentValue = 0
    var score = 0
    var roundNumber = 0
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let thumbImageNormal = UIImage(named: "star2")!
        slider.setThumbImage(thumbImageNormal, for: UIControlState())
        let thumbImageHighlighted = UIImage(named: "star2")!
        slider.setThumbImage(thumbImageHighlighted, for: .highlighted)
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right:14)
        
        let trackLeftImage = UIImage(named: "SliderTrackLeft Yellow")!
        let trackLeftResizable = trackLeftImage.resizableImage(withCapInsets: insets)
        slider.setMinimumTrackImage(trackLeftResizable, for: UIControlState())
        let trackRightImage = UIImage(named: "SliderTrackRight")!
        let trackRightResizable = trackRightImage.resizableImage(withCapInsets: insets)
        slider.setMaximumTrackImage(trackRightResizable, for: UIControlState())
        
        startNewGame()
        updateLabels()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func showAlert() {
        let difference = abs(targetValue - currentValue)
        var points = 100 - difference
        if (points == 100) {
            points = 200;
        }
        score += points
        let message = "You scored: \(points) points"
        
        var title: String = "";
        if (points == 200 ) {
            title = "PERFECT! DOUBLE POINTS!"
        } else if (points > 89 && points < 100) {
            title = "You almost had it!"
        } else if (points > 69 && points < 90) {
            title = "Pretty good."
        } else if (points > 49 && points < 70) {
            title = "So so."
        } else  {
            title = "Not even close..."
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "awesome", style: .default, handler: { action in self.startNewRound()
            self.updateLabels()
        })
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sliderMoved(_ slider: UISlider) {
        currentValue = lroundf(slider.value)
    }
    
    func startNewRound() {
        roundNumber += 1;
        currentValue = 50
        slider.value = Float(currentValue)
        targetValue = 1 + Int(arc4random_uniform(100));
        
    }
    func updateLabels() {
        targetLabel.text = String(targetValue)
        scoreLabel.text = String(score)
        roundLabel.text = String(roundNumber)
    }
    
    func startNewGame() {
        score = 0
        roundNumber = 0
        startNewRound()
    }
    
    @IBAction func startOver() {
        startNewGame()
        updateLabels()
        
        let transition = CATransition()
        transition.type = kCATransitionFade
        transition.duration = 1
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        view.layer.add(transition, forKey: nil)
    }
    
}

